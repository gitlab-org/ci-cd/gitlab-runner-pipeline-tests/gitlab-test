#!/bin/sh

term() {
	echo "Caught SIGTERM"
	sleep 3
	echo "Exiting $$"
	exit 0
}

trap term TERM
trap term INT

echo "Starting $$"

for i in $(seq 1 30); do
	echo "$i"
	sleep 1
done
